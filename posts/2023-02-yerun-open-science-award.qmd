---
title: "The ROCK won a YERUN Open Science Award"
author: Gjalt-Jorn Peters & Szilvia Zörgő
date: '2023-02-14'
draft: false
slug: "yerun-open-science-award"
categories: []
tags: []
---

The Young European Research Universities Network has bestowed a YERUN Open Science award on the Reproducible Open Coding Kit! 🎉

![](../img/the-rock---celebrate.gif){width=40% fig-alt="A GIF of Dwayne Johnson (The Rock) saying 'The Rock wantes to help you celebrate this very joyous occasion!'"}

In fierce competition with 32 other excellent Open Science initiatives, the ROCK joined four other projects (the LERO Open Science Committee at the University of Limerick, the Open Knowledge Initiative by the KIOS Centre of Excellence, Advancing Registered Reports in Organisational Research, and the Theodor Fontane Archive) in the award ceremony on the 14th of February.

The award ceremony was recorded and is available from [the YERUN press release](https://yerun.eu/2023/02/yerun-open-science-awards-2022-congratulations-to-the-winners/){.external target="_blank"}.

