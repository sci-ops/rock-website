---
title: "Play"
engine: knitr
filters:
  - webr
webr: 
  show-startup-message: true
  packages: ['rock']
---

```{webr-r}
fit = lm(mpg ~ am, data = mtcars)

summary(fit)
```

