### In Plesk, execute with:
### bash deploy.sh >> deployment.log 2>&1
### Cran job for executing every hour with logging
### @hourly bash ~/git_clone_hpss/deploy.sh >> ~/git_clone_hpss/deployment.log 2>&1
### https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/
### To edit the cron file with nano instead of vim:
### export VISUAL=nano; crontab -e

echo ----------
echo $(date)

echo Showing and resetting path ...

echo Path: $PATH
#export R_HOME="/usr/local/bin/R"
#export R_HOME="/usr/lib64/R/bin/R"

### Set the path so LaTeX can be found
PATH=$PATH:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

echo Path: $PATH

### Go to directory with cloned git repo
cd ~/deploy_rock.science

echo Running Quarto...

### Render the site
/usr/local/bin/quarto render

echo Done with Quarto. Deleting old directories and files...

### Delete all contents in public HTML directory
rm -rf ~/rock.science/*.*
rm -rf ~/rock.science/*
rm -f ~/rock.science/.htaccess

echo Done deleting old directories and files. Copying over new website...

### Copy website
cp -RT public ~/rock.science

### Copy .htaccess
cp .htaccess ~/rock.science

echo Done copying over new website.

echo ----------
