---
title: "🎯 Vision"
---

[https://www.extension.iastate.edu/agdm/wholefarm/html/c5-09.html](https://www.extension.iastate.edu/agdm/wholefarm/html/c5-09.html)

## The ROCK vision

The Reproducible Open Coding Kit is a human- and machine-readable standard for coded qualitative data that fosters sharing, collaboration and interaction.

## The ROCK mission

...

## The ROCK Core Values

...

### As open as possible, as closed as necessary.

In everything, we try to be as open as possible. This openness manifests in a number of ways.

#### Openness as transparency

We strive for optimal transparency in everything we do. We do our best to consistently use open standards and work in public.

#### Openness as accessibility

We aim to be [optimally accessible to everybody](https://www.w3.org/standards/webdesign/accessibility){.external target="_blank"}. For example, we strive to take into account colorblindness, screen readers, and other variations. In addition, the products we produce are available freely.

#### Openness as interoperability

We aim to make it as easy as possible to interact with ROCK documents and convert to and from different formats.

### Hackable

It's as easy as possible for people to develop tools for working with ROCK data.

### Community-owned

We strive to shape and maintain a governance for the ROCK standard that will ensure it remaining a community-owned project.

### Fostering methodological innovation

We aim to develop the ROCK so that it lends itself to as many uses as possible, inspiring methodological innovation.

### Minimally prescriptive

We aim to develop the ROCK to be usable regardless of one's perspective on philosophy of science and methodology.

### Catmatic

We are not dogmatic.

## The ROCK Strategies

- Document the ROCK standard
- Establish and maintain a governance structure for the ROCK
- Produce various software packages for working with ROCK data

## The ROCK Goals

- Create the new iROCK
- Create an R package that can import from / export to the [REFI-QDA standard](https://www.qdasoftware.org/){.external target="_blank"}
- Finish the ROCK book
- Create a series of training materials to work with the ROCK

## The ROCK Objectives

The ROCK objectives are managed in several separate projects.

